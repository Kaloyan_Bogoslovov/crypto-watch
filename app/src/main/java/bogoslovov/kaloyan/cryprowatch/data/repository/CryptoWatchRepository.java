package bogoslovov.kaloyan.cryprowatch.data.repository;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.greendao.query.Query;

import java.util.List;

import bogoslovov.kaloyan.cryprowatch.data.CryptoWatchService;
import bogoslovov.kaloyan.cryprowatch.data.db.Coin;
import bogoslovov.kaloyan.cryprowatch.data.db.CoinDao;
import bogoslovov.kaloyan.cryprowatch.data.db.DaoSession;
import bogoslovov.kaloyan.cryprowatch.data.db.Position;
import bogoslovov.kaloyan.cryprowatch.data.db.PositionDao;
import bogoslovov.kaloyan.cryprowatch.data.models.getcoinusd.CoinUsdModel;
import bogoslovov.kaloyan.cryprowatch.features.details.DetailsEvent;
import bogoslovov.kaloyan.cryprowatch.features.newposition.NewPositionEvent;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static bogoslovov.kaloyan.cryprowatch.utils.Constants.COIN_DATA_NOT_AVAILABLE;
import static bogoslovov.kaloyan.cryprowatch.utils.Constants.COIN_LOADED;
import static bogoslovov.kaloyan.cryprowatch.utils.Constants.DETAILS_FRAGMENT_ID;
import static bogoslovov.kaloyan.cryprowatch.utils.Constants.NEW_POSITION_FRAGMENT_ID;

public class CryptoWatchRepository implements CryptoWatchDataSource {
    private static CryptoWatchRepository INSTANCE = null;

    private DaoSession mDaoSession;

    private CryptoWatchRepository(DaoSession daoSession) {
        mDaoSession = daoSession;
    }

    public static CryptoWatchRepository getInstance(DaoSession daoSession) {

        if (INSTANCE == null) {
            INSTANCE = new CryptoWatchRepository(daoSession);
        }

        return INSTANCE;
    }

    //Database

    @Override
    public List<Coin> getAllCoins() {
        Query<Coin> expenseQuery = mDaoSession.getCoinDao().queryBuilder().build();
        return expenseQuery.list();
    }

    @Override
    public void insertCoin(Coin coin) {
        mDaoSession.getCoinDao().insert(coin);
    }

    @Override
    public void updateCoin(Coin coin) {
        mDaoSession.getCoinDao().update(coin);
    }

    @Override
    public List<Coin> getFavouriteCoins() {
        return mDaoSession.getCoinDao().queryBuilder().where(CoinDao.Properties.IsFavourite.eq(true)).list();
    }

    @Override
    public List<Coin> getCoinById(Long id) {
        return mDaoSession.getCoinDao().queryBuilder().where(CoinDao.Properties.Id.eq(id)).list();
    }

    @Override
    public long insertPosition(Position position) {
        return mDaoSession.getPositionDao().insert(position);
    }

    @Override
    public void updatePosition(Position position) {
        mDaoSession.getPositionDao().update(position);
    }

    @Override
    public List<Position> getAllPositions() {
        Query<Position> expenseQuery = mDaoSession.getPositionDao().queryBuilder().build();
        return expenseQuery.list();
    }

    @Override
    public void deletePositionById(Long id) {
        mDaoSession.getPositionDao().deleteByKey(id);
    }

    //REST

    @Override
    public void getCoinByServerId(int serverId, final int fragmentId) {
        CryptoWatchService service = CryptoWatchService.Factory.getInstance();
        service.getCoinPriceInUsd(serverId).enqueue(new Callback<CoinUsdModel>() {
            @Override
            public void onResponse(Call<CoinUsdModel> call, Response<CoinUsdModel> response) {
                if (response.code() == 200) {
                    CoinUsdModel coinUsdModel = response.body();
                    postSuccessfulResponse(fragmentId, coinUsdModel);
                }
            }

            @Override
            public void onFailure(Call<CoinUsdModel> call, Throwable t) {
                postFailedResponse(fragmentId);
            }
        });
    }

    private void postSuccessfulResponse(int fragmentId, CoinUsdModel coinUsdModel) {
        switch (fragmentId) {
            case NEW_POSITION_FRAGMENT_ID:
                EventBus.getDefault().post(new NewPositionEvent(COIN_LOADED,coinUsdModel));
                break;
            case DETAILS_FRAGMENT_ID:
                EventBus.getDefault().post(new DetailsEvent(COIN_LOADED,coinUsdModel));
                break;
        }
    }

    private void postFailedResponse(int fragmentId) {
        switch (fragmentId) {
            case NEW_POSITION_FRAGMENT_ID:
                EventBus.getDefault().post(new NewPositionEvent(COIN_DATA_NOT_AVAILABLE,null));
                break;
            case DETAILS_FRAGMENT_ID:
                EventBus.getDefault().post(new DetailsEvent(COIN_DATA_NOT_AVAILABLE,null));
                break;
        }
    }
}
