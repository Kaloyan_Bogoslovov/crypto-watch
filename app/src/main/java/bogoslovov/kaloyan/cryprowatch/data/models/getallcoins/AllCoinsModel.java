package bogoslovov.kaloyan.cryprowatch.data.models.getallcoins;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class AllCoinsModel {

    @SerializedName("data")
    @Expose
    private List<CoinModel> data = null;

    @SerializedName("metadata")
    @Expose
    private Metadata metadata;

    public AllCoinsModel() {
    }

    public List<CoinModel> getData() {
        return data;
    }

    public void setData(List<CoinModel> data) {
        this.data = data;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }
}
