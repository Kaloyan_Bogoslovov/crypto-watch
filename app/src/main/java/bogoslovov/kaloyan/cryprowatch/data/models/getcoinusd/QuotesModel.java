
package bogoslovov.kaloyan.cryprowatch.data.models.getcoinusd;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class QuotesModel {

    @SerializedName("USD")
    @Expose
    private UsdModel usd;

    public UsdModel getUSD() {
        return usd;
    }

    public void setUSD(UsdModel usd) {
        this.usd = usd;
    }

}
