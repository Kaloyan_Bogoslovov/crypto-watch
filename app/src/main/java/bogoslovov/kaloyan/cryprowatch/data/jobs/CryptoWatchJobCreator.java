package bogoslovov.kaloyan.cryprowatch.data.jobs;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobCreator;

public class CryptoWatchJobCreator implements JobCreator {

    @Override
    public Job create(String tag) {
        switch (tag){
            case CoinsJob.TAG:
                return new CoinsJob();
            case PortfolioJob.TAG:
                return new PortfolioJob();
        }
        return null;
    }

}
