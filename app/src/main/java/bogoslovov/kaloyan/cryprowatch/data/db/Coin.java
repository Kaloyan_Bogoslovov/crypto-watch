package bogoslovov.kaloyan.cryprowatch.data.db;

import android.os.Parcel;
import android.os.Parcelable;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class Coin implements Parcelable{

    @Id(autoincrement = true)
    private Long id;

    private int serverId;
    private Long portfolioId;
    private String coinName;
    private String coinSymbol;
    private boolean isFavourite;
    private boolean isInPortfolio;

    @Generated(hash = 2144260277)
    public Coin(Long id, int serverId, Long portfolioId, String coinName,
            String coinSymbol, boolean isFavourite, boolean isInPortfolio) {
        this.id = id;
        this.serverId = serverId;
        this.portfolioId = portfolioId;
        this.coinName = coinName;
        this.coinSymbol = coinSymbol;
        this.isFavourite = isFavourite;
        this.isInPortfolio = isInPortfolio;
    }

    @Generated(hash = 1629455692)
    public Coin() {
    }

    protected Coin(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readLong();
        }
        serverId = in.readInt();
        if (in.readByte() == 0) {
            portfolioId = null;
        } else {
            portfolioId = in.readLong();
        }
        coinName = in.readString();
        coinSymbol = in.readString();
        isFavourite = in.readByte() != 0;
        isInPortfolio = in.readByte() != 0;
    }

    public static final Creator<Coin> CREATOR = new Creator<Coin>() {
        @Override
        public Coin createFromParcel(Parcel in) {
            return new Coin(in);
        }

        @Override
        public Coin[] newArray(int size) {
            return new Coin[size];
        }
    };

    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public int getServerId() {
        return this.serverId;
    }
    public void setServerId(int serverId) {
        this.serverId = serverId;
    }
    public Long getPortfolioId() {
        return this.portfolioId;
    }
    public void setPortfolioId(Long portfolioId) {
        this.portfolioId = portfolioId;
    }
    public String getCoinName() {
        return this.coinName;
    }
    public void setCoinName(String coinName) {
        this.coinName = coinName;
    }
    public String getCoinSymbol() {
        return this.coinSymbol;
    }
    public void setCoinSymbol(String coinSymbol) {
        this.coinSymbol = coinSymbol;
    }
    public boolean getIsFavourite() {
        return this.isFavourite;
    }
    public void setIsFavourite(boolean isFavourite) {
        this.isFavourite = isFavourite;
    }
    public boolean getIsInPortfolio() {
        return this.isInPortfolio;
    }
    public void setIsInPortfolio(boolean isInPortfolio) {
        this.isInPortfolio = isInPortfolio;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(id);
        }
        dest.writeInt(serverId);
        if (portfolioId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(portfolioId);
        }
        dest.writeString(coinName);
        dest.writeString(coinSymbol);
        dest.writeByte((byte) (isFavourite ? 1 : 0));
        dest.writeByte((byte) (isInPortfolio ? 1 : 0));
    }
}
