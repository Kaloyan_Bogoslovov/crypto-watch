package bogoslovov.kaloyan.cryprowatch.data;

import bogoslovov.kaloyan.cryprowatch.BuildConfig;
import bogoslovov.kaloyan.cryprowatch.data.models.getallcoins.AllCoinsModel;
import bogoslovov.kaloyan.cryprowatch.data.models.getcoinusd.CoinUsdModel;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface CryptoWatchService {

    @GET("listings/")
    Call<AllCoinsModel> getAllCoins();

    @GET("ticker/{coinId}/")
    Call<CoinUsdModel> getCoinPriceInUsd(@Path("coinId")int coinId);

    class Factory {

        private static CryptoWatchService service;
        private static String BASE_URL = "https://api.coinmarketcap.com/v2/";

        public static CryptoWatchService getInstance() {
            if (service == null) {
                OkHttpClient.Builder builder = new OkHttpClient().newBuilder();

                if (BuildConfig.DEBUG) {
                    builder.addInterceptor(new HttpLoggingInterceptor().setLevel(
                            HttpLoggingInterceptor.Level.BODY));
                }

                Retrofit retrofit = new Retrofit.Builder()
                        .client(builder.build())
                        .baseUrl(BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                service = retrofit.create(CryptoWatchService.class);
                return service;
            } else {
                return service;
            }
        }
    }
}
