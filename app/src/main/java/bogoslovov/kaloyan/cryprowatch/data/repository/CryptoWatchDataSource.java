package bogoslovov.kaloyan.cryprowatch.data.repository;

import java.util.List;

import bogoslovov.kaloyan.cryprowatch.data.db.Coin;
import bogoslovov.kaloyan.cryprowatch.data.db.Position;

public interface CryptoWatchDataSource {

    //database

    List<Coin> getAllCoins();

    void insertCoin(Coin coin);

    void updateCoin(Coin coin);

    List <Coin> getFavouriteCoins();

    List <Coin> getCoinById(Long id);

    long insertPosition(Position position);

    void updatePosition(Position position);

    List<Position> getAllPositions();

    void deletePositionById(Long id);

    //rest

    void getCoinByServerId(int serverId, int fragmentId);
}
