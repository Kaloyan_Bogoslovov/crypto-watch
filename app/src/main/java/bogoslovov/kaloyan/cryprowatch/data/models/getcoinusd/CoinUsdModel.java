
package bogoslovov.kaloyan.cryprowatch.data.models.getcoinusd;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CoinUsdModel {

    @SerializedName("data")
    @Expose
    private DataModel dataModel;
    @SerializedName("metadata")
    @Expose
    private Metadata metadata;

    public DataModel getDataModel() {
        return dataModel;
    }

    public void setDataModel(DataModel dataModel) {
        this.dataModel = dataModel;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

}
