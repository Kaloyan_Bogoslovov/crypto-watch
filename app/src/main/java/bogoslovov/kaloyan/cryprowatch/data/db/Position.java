package bogoslovov.kaloyan.cryprowatch.data.db;

import android.os.Parcel;
import android.os.Parcelable;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class Position implements Parcelable{

    @Id(autoincrement = true)
    private Long id;
    private int coinServerId;

    private String coinName;
    private String coinSymbol;

    private Double amountPurchased;
    private Double purchasedAtPrice;

    private Double lastUpdatedPrice;
    private int lastUpdatedTime;
    @Generated(hash = 1121190257)
    public Position(Long id, int coinServerId, String coinName, String coinSymbol,
            Double amountPurchased, Double purchasedAtPrice,
            Double lastUpdatedPrice, int lastUpdatedTime) {
        this.id = id;
        this.coinServerId = coinServerId;
        this.coinName = coinName;
        this.coinSymbol = coinSymbol;
        this.amountPurchased = amountPurchased;
        this.purchasedAtPrice = purchasedAtPrice;
        this.lastUpdatedPrice = lastUpdatedPrice;
        this.lastUpdatedTime = lastUpdatedTime;
    }
    @Generated(hash = 958937587)
    public Position() {
    }

    protected Position(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readLong();
        }
        coinServerId = in.readInt();
        coinName = in.readString();
        coinSymbol = in.readString();
        if (in.readByte() == 0) {
            amountPurchased = null;
        } else {
            amountPurchased = in.readDouble();
        }
        if (in.readByte() == 0) {
            purchasedAtPrice = null;
        } else {
            purchasedAtPrice = in.readDouble();
        }
        if (in.readByte() == 0) {
            lastUpdatedPrice = null;
        } else {
            lastUpdatedPrice = in.readDouble();
        }
        lastUpdatedTime = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(id);
        }
        dest.writeInt(coinServerId);
        dest.writeString(coinName);
        dest.writeString(coinSymbol);
        if (amountPurchased == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(amountPurchased);
        }
        if (purchasedAtPrice == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(purchasedAtPrice);
        }
        if (lastUpdatedPrice == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(lastUpdatedPrice);
        }
        dest.writeInt(lastUpdatedTime);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Position> CREATOR = new Creator<Position>() {
        @Override
        public Position createFromParcel(Parcel in) {
            return new Position(in);
        }

        @Override
        public Position[] newArray(int size) {
            return new Position[size];
        }
    };

    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public int getCoinServerId() {
        return this.coinServerId;
    }
    public void setCoinServerId(int coinServerId) {
        this.coinServerId = coinServerId;
    }
    public String getCoinName() {
        return this.coinName;
    }
    public void setCoinName(String coinName) {
        this.coinName = coinName;
    }
    public String getCoinSymbol() {
        return this.coinSymbol;
    }
    public void setCoinSymbol(String coinSymbol) {
        this.coinSymbol = coinSymbol;
    }
    public Double getAmountPurchased() {
        return this.amountPurchased;
    }
    public void setAmountPurchased(Double amountPurchased) {
        this.amountPurchased = amountPurchased;
    }
    public Double getPurchasedAtPrice() {
        return this.purchasedAtPrice;
    }
    public void setPurchasedAtPrice(Double purchasedAtPrice) {
        this.purchasedAtPrice = purchasedAtPrice;
    }
    public Double getLastUpdatedPrice() {
        return this.lastUpdatedPrice;
    }
    public void setLastUpdatedPrice(Double lastUpdatedPrice) {
        this.lastUpdatedPrice = lastUpdatedPrice;
    }
    public int getLastUpdatedTime() {
        return this.lastUpdatedTime;
    }
    public void setLastUpdatedTime(int lastUpdatedTime) {
        this.lastUpdatedTime = lastUpdatedTime;
    }

    
}
