package bogoslovov.kaloyan.cryprowatch.data.jobs;

import android.support.annotation.NonNull;
import android.util.Log;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobRequest;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import bogoslovov.kaloyan.cryprowatch.common.CryptoWatchBaseApp;
import bogoslovov.kaloyan.cryprowatch.features.search.SearchEvent;
import bogoslovov.kaloyan.cryprowatch.data.CryptoWatchService;
import bogoslovov.kaloyan.cryprowatch.data.db.Coin;
import bogoslovov.kaloyan.cryprowatch.data.models.getallcoins.AllCoinsModel;
import bogoslovov.kaloyan.cryprowatch.data.models.getallcoins.CoinModel;
import bogoslovov.kaloyan.cryprowatch.data.repository.CryptoWatchDataSource;
import bogoslovov.kaloyan.cryprowatch.data.repository.CryptoWatchRepository;
import bogoslovov.kaloyan.cryprowatch.utils.Constants;
import retrofit2.Response;

import static bogoslovov.kaloyan.cryprowatch.utils.Constants.NO_PORTFOLIO_ID;


public class CoinsJob extends Job {

    public static final String TAG = "CoinsJob";

    public static void scheduleJobNow() {
        new JobRequest.Builder(CoinsJob.TAG)
                .setExact(TimeUnit.SECONDS.toMillis(1))
                .setBackoffCriteria(TimeUnit.SECONDS.toMillis(30),
                        JobRequest.BackoffPolicy.EXPONENTIAL)
                .setUpdateCurrent(true)
                .build()
                .schedule();
    }

    @NonNull
    @Override
    protected Result onRunJob(Params params) {
        Result result;
        try {
            result = syncCoins();
        } catch (IOException e) {
            result = Result.FAILURE;
            EventBus.getDefault().post(new SearchEvent(Constants.COINS_SYNC_FAILED));
        }
        return result;
    }

    private Result syncCoins() throws IOException {
        CryptoWatchService service = CryptoWatchService.Factory.getInstance();
        Response<AllCoinsModel> response = service.getAllCoins().execute();

        if (response.isSuccessful()) {
            insertCoinsIntoDB(response);
            EventBus.getDefault().post(new SearchEvent(Constants.COINS_SYNC_SUCCESSFUL));
            return Result.SUCCESS;
        } else {
            EventBus.getDefault().post(new SearchEvent(Constants.COINS_SYNC_FAILED));
            return Result.FAILURE;
        }
    }

    private void insertCoinsIntoDB(Response<AllCoinsModel> response){
        CryptoWatchDataSource repository =
                CryptoWatchRepository.getInstance(CryptoWatchBaseApp.getDaoSession());

        List<Coin> coins = getCoinsFromResponse(response.body().getData());

        Set<Integer> serverIds = getServerIds(repository.getAllCoins());

        for (Coin coin : coins) {
            if (serverIds.contains(coin.getServerId())){
                serverIds.remove(coin.getServerId());
            } else {
                if (CryptoWatchBaseApp.Permissions.localLog) Log.i(TAG,coin.getCoinName());
                coin.setPortfolioId(NO_PORTFOLIO_ID);
                repository.insertCoin(coin);
            }
        }
    }

    private List<Coin> getCoinsFromResponse(List<CoinModel> models) {
        List<Coin> coins = new ArrayList<>();

        for (CoinModel model : models) {
            Coin coin = new Coin();
            coin.setCoinName(model.getName());
            coin.setCoinSymbol(model.getSymbol());
            coin.setServerId(model.getId());
            coin.setIsFavourite(false);
            coins.add(coin);
        }

        return coins;
    }

    private Set<Integer> getServerIds(List<Coin> coinsInDatabase){
        Set<Integer> serverIds = new HashSet<Integer>();

        for (Coin coin : coinsInDatabase) {
            serverIds.add(coin.getServerId());
        }

        return serverIds;
    }

}
