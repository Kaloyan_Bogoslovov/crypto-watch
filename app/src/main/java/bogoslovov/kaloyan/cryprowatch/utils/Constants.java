package bogoslovov.kaloyan.cryprowatch.utils;

public class Constants {

    //Search EventBus Events
    public static final int COINS_SYNC_SUCCESSFUL = 1;
    public static final int COINS_SYNC_FAILED = 2;

    //Details EventBus Events
    public static final int COIN_LOADED = 3;
    public static final int COIN_DATA_NOT_AVAILABLE = 4;

    //Extras
    public static final String COIN_ID_EXTRA = "coin-id";
    public static final String COIN_NAME_EXTRA = "coin-name";
    public static final String COIN_EXTRA = "coin-extra";

    //Event bus fragmentIds
    public static final int NEW_POSITION_FRAGMENT_ID = 5;
    public static final int DETAILS_FRAGMENT_ID = 6;

    public static final long NO_PORTFOLIO_ID = 0L;
}
