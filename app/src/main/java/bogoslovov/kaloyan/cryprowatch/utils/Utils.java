package bogoslovov.kaloyan.cryprowatch.utils;

import android.support.annotation.Nullable;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Utils {

    /**
     * Ensures that an object reference passed as a parameter to the calling method is not null.
     *
     * @param reference an object reference
     * @return the non-null reference that was validated
     * @throws NullPointerException if {@code reference} is null
     */
    public static <T> T checkNotNull(T reference) {
        if (reference == null) {
            throw new NullPointerException();
        }
        return reference;
    }

    /**
     * Ensures that an object reference passed as a parameter to the calling method is not null.
     *
     * @param reference an object reference
     * @param errorMessage the exception message to use if the check fails; will be converted to a
     *     string using {@link String#valueOf(Object)}
     * @return the non-null reference that was validated
     * @throws NullPointerException if {@code reference} is null
     */
    public static <T> T checkNotNull(T reference, @Nullable Object errorMessage) {
        if (reference == null) {
            throw new NullPointerException(String.valueOf(errorMessage));
        }
        return reference;
    }

    public static String minimizeNumber(double number) {
        if (number > 1000000) {
            BigDecimal minimizedNumber = new BigDecimal(number/1000000);
            minimizedNumber = minimizedNumber.setScale(3, RoundingMode.HALF_UP);
            return minimizedNumber + "M";
        } else {
            return String.valueOf(number);
        }
    }
}
