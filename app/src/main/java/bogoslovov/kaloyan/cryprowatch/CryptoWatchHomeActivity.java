package bogoslovov.kaloyan.cryprowatch;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import bogoslovov.kaloyan.cryprowatch.common.CryptoWatchBaseApp;
import bogoslovov.kaloyan.cryprowatch.data.db.DaoSession;
import bogoslovov.kaloyan.cryprowatch.data.repository.CryptoWatchRepository;
import bogoslovov.kaloyan.cryprowatch.features.favourites.FavouritesFragment;
import bogoslovov.kaloyan.cryprowatch.features.favourites.FavouritesPresenter;
import bogoslovov.kaloyan.cryprowatch.features.portfolio.PortfolioFragment;
import bogoslovov.kaloyan.cryprowatch.features.portfolio.PortfolioPresenter;
import bogoslovov.kaloyan.cryprowatch.features.search.SearchFragment;
import bogoslovov.kaloyan.cryprowatch.features.search.SearchPresenter;
import bogoslovov.kaloyan.cryprowatch.features.settings.SettingsFragment;
import bogoslovov.kaloyan.cryprowatch.features.settings.SettingsPresenter;
import bogoslovov.kaloyan.cryprowatch.utils.ActivityUtils;

public class CryptoWatchHomeActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    private DaoSession mDaoSession;
    private FragmentManager mFragmentManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crypto_watch_home);

        BottomNavigationView navigationView = findViewById(R.id.navigation);
        navigationView.setOnNavigationItemSelectedListener(this);

        mDaoSession = CryptoWatchBaseApp.getDaoSession();

        mFragmentManager = getFragmentManager();
        Fragment fragment = mFragmentManager.findFragmentById(R.id.contentFrame);


        if (fragment == null){
            startPortfolioFragment(null);
        } else {
            if (fragment instanceof SearchFragment){
                startSearchFragment(fragment);
            } else if (fragment instanceof PortfolioFragment) {
                startPortfolioFragment(fragment);
            } else if (fragment instanceof FavouritesFragment) {
                startFavouritesFragment(fragment);
            } else if (fragment instanceof SettingsFragment) {
                startSettingsFragment(fragment);
            }
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        Fragment fragment = mFragmentManager.findFragmentById(R.id.contentFrame);

        switch (item.getItemId()) {
            case R.id.action_portfolio:
                startPortfolioFragment(fragment);
                break;
            case R.id.action_search:
                startSearchFragment(fragment);
                break;
            case R.id.action_favourites:
                startFavouritesFragment(fragment);
                break;
            case R.id.action_settings:
                startSettingsFragment(fragment);
                break;
        }

        return true;
    }

    private void startPortfolioFragment(Fragment fragment){
        if (fragment != null && fragment instanceof PortfolioFragment){
            new PortfolioPresenter(CryptoWatchRepository.getInstance(mDaoSession), (PortfolioFragment) fragment);
        } else {
            PortfolioFragment portfolioFragment = PortfolioFragment.newInstance();
            ActivityUtils.addFragmentToActivity(mFragmentManager, portfolioFragment, R.id.contentFrame);

            new PortfolioPresenter(CryptoWatchRepository.getInstance(mDaoSession), portfolioFragment);
        }
    }

    private void startSearchFragment(Fragment fragment){
        if (fragment != null && fragment instanceof SearchFragment) {
            new SearchPresenter(CryptoWatchRepository.getInstance(mDaoSession), (SearchFragment)fragment);
        } else {
            SearchFragment searchFragment = SearchFragment.newInstance();
            ActivityUtils.addFragmentToActivity(mFragmentManager, searchFragment, R.id.contentFrame);

            new SearchPresenter(CryptoWatchRepository.getInstance(mDaoSession), searchFragment);
        }
    }

    private void startFavouritesFragment(Fragment fragment){
        if (fragment != null && fragment instanceof FavouritesFragment){
            new FavouritesPresenter(CryptoWatchRepository.getInstance(mDaoSession), (FavouritesFragment)fragment);
        } else {
            FavouritesFragment favouritesFragment = FavouritesFragment.newInstance();
            ActivityUtils.addFragmentToActivity(mFragmentManager, favouritesFragment, R.id.contentFrame);

            new FavouritesPresenter(CryptoWatchRepository.getInstance(mDaoSession), favouritesFragment);
        }
    }

    private void startSettingsFragment(Fragment fragment){
        if (fragment !=null && fragment instanceof SettingsFragment) {
            new SettingsPresenter(CryptoWatchRepository.getInstance(mDaoSession), (SettingsFragment) fragment);
        } else {
            SettingsFragment settingsFragment = SettingsFragment.newInstance();
            ActivityUtils.addFragmentToActivity(mFragmentManager, settingsFragment, R.id.contentFrame);

            new SettingsPresenter(CryptoWatchRepository.getInstance(mDaoSession), settingsFragment);
        }
    }
}
