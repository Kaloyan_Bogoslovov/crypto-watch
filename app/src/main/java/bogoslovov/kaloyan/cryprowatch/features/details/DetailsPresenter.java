package bogoslovov.kaloyan.cryprowatch.features.details;

import java.util.List;

import bogoslovov.kaloyan.cryprowatch.data.db.Coin;
import bogoslovov.kaloyan.cryprowatch.data.repository.CryptoWatchDataSource;
import bogoslovov.kaloyan.cryprowatch.utils.Utils;

import static bogoslovov.kaloyan.cryprowatch.utils.Constants.DETAILS_FRAGMENT_ID;

public class DetailsPresenter implements DetailsContract.Presenter{

    private static final String TAG = DetailsPresenter.class.getSimpleName();

    private final CryptoWatchDataSource mCryptoWatchRepository;
    private final DetailsContract.View mDetailsView;

    public DetailsPresenter(CryptoWatchDataSource cryptoWatchRepository, DetailsContract.View detailsView) {
        mCryptoWatchRepository = Utils.checkNotNull(cryptoWatchRepository, "cryptoWatchRepository cannot be null");
        mDetailsView = Utils.checkNotNull(detailsView, "detailsView cannot be null");

        mDetailsView.setPresenter(this);
    }

    @Override
    public void loadCoinById(Long id) {
        List<Coin> coin = mCryptoWatchRepository.getCoinById(id);
        mCryptoWatchRepository.getCoinByServerId(coin.get(0).getServerId(), DETAILS_FRAGMENT_ID);
    }

    @Override
    public String getTime(int lastUpdated) {
        java.util.Date time = new java.util.Date((long)lastUpdated*1000);
        return time.getHours()+":"+time.getMinutes()+":"+time.getSeconds()+" Currency in USD";
    }

    @Override
    public boolean isPriceMovementUpwards(Double priceChange) {
        return priceChange > 0;
    }

    @Override
    public boolean isSupplyDataAvailable(Double circulatingSupply, Double maxSupply) {
        return circulatingSupply != null && maxSupply != null;
    }
}
