package bogoslovov.kaloyan.cryprowatch.features.newposition;

import bogoslovov.kaloyan.cryprowatch.BaseView;

public interface NewPositionContract {

    interface View extends BaseView<NewPositionContract.Presenter> {

        void onPositionAdded();
    }

    interface Presenter {

        void addNewPosition(double amount, double price, Long coinId);
    }
}
