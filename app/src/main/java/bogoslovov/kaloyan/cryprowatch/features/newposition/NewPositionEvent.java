package bogoslovov.kaloyan.cryprowatch.features.newposition;

import bogoslovov.kaloyan.cryprowatch.data.models.getcoinusd.CoinUsdModel;

public class NewPositionEvent {

    private final int event;
    private final CoinUsdModel coinUsdModel;

    public NewPositionEvent(int event, CoinUsdModel coinUsdModel) {
        this.event = event;
        this.coinUsdModel = coinUsdModel;
    }

    public int getEvent() {
        return event;
    }

    public CoinUsdModel getCoinUsdModel() {
        return coinUsdModel;
    }
}
