package bogoslovov.kaloyan.cryprowatch.features.details;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import bogoslovov.kaloyan.cryprowatch.R;
import bogoslovov.kaloyan.cryprowatch.common.CryptoWatchBaseApp;
import bogoslovov.kaloyan.cryprowatch.data.db.DaoSession;
import bogoslovov.kaloyan.cryprowatch.data.repository.CryptoWatchRepository;
import bogoslovov.kaloyan.cryprowatch.utils.ActivityUtils;

import static bogoslovov.kaloyan.cryprowatch.utils.Constants.COIN_ID_EXTRA;

public class DetailsActivity extends AppCompatActivity{

    private Bundle mCoinIdBundle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        if (savedInstanceState == null) {
            mCoinIdBundle = getIntent().getExtras();
        } else {
            mCoinIdBundle = savedInstanceState;
        }

        DetailsFragment detailsFragment =
                (DetailsFragment) getFragmentManager().findFragmentById(R.id.contentFrame);

        if (detailsFragment == null) {
            detailsFragment = DetailsFragment.newInstance();
            detailsFragment.setArguments(mCoinIdBundle);
            ActivityUtils.addFragmentToActivity(getFragmentManager(), detailsFragment, R.id.contentFrame);
        }

        DaoSession daoSession = CryptoWatchBaseApp.getDaoSession();

        new DetailsPresenter(CryptoWatchRepository.getInstance(daoSession), detailsFragment);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putLong(COIN_ID_EXTRA, mCoinIdBundle.getLong(COIN_ID_EXTRA));
        super.onSaveInstanceState(outState);
    }
}
