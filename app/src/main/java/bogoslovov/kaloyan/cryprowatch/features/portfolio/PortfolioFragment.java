package bogoslovov.kaloyan.cryprowatch.features.portfolio;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import bogoslovov.kaloyan.cryprowatch.R;
import bogoslovov.kaloyan.cryprowatch.features.favourites.FavouritesContract;
import bogoslovov.kaloyan.cryprowatch.features.favourites.FavouritesFragment;
import bogoslovov.kaloyan.cryprowatch.utils.Utils;

public class PortfolioFragment extends Fragment implements PortfolioContract.View{

    private static final String TAG = FavouritesFragment.class.getSimpleName();

    private PortfolioContract.Presenter mPresenter;

    public PortfolioFragment() {
    }

    public static PortfolioFragment newInstance(){
        return new PortfolioFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_portfolio, container, false);

        return view;
    }

    @Override
    public void setPresenter(PortfolioContract.Presenter presenter) {
        mPresenter = Utils.checkNotNull(presenter);
    }
}
