package bogoslovov.kaloyan.cryprowatch.features.search;

public class SearchEvent {

    private final int event;

    public SearchEvent(int event) {
        this.event = event;
    }

    public int getEvent() {
        return event;
    }
}
