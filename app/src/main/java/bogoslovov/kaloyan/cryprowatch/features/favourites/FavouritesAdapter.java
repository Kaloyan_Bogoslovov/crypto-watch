package bogoslovov.kaloyan.cryprowatch.features.favourites;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import bogoslovov.kaloyan.cryprowatch.R;
import bogoslovov.kaloyan.cryprowatch.common.CryptoWatchBaseApp;
import bogoslovov.kaloyan.cryprowatch.data.db.Coin;
import bogoslovov.kaloyan.cryprowatch.data.repository.CryptoWatchDataSource;
import bogoslovov.kaloyan.cryprowatch.data.repository.CryptoWatchRepository;
import bogoslovov.kaloyan.cryprowatch.features.details.DetailsActivity;
import bogoslovov.kaloyan.cryprowatch.features.newposition.NewPositionActivity;
import bogoslovov.kaloyan.cryprowatch.features.search.SearchAdapter;

import static bogoslovov.kaloyan.cryprowatch.utils.Constants.COIN_EXTRA;
import static bogoslovov.kaloyan.cryprowatch.utils.Constants.COIN_ID_EXTRA;
import static bogoslovov.kaloyan.cryprowatch.utils.Constants.COIN_NAME_EXTRA;

public class FavouritesAdapter extends RecyclerView.Adapter<FavouritesViewHolder>{

    private List<Coin> mCoins;
    private Context mContext;
    private CryptoWatchDataSource mRepository;
    private SearchAdapter.PortfolioListener mPortfolioListener;

    public FavouritesAdapter(List<Coin> mCoins, Context context, SearchAdapter.PortfolioListener listener) {
        this.mCoins = mCoins;
        mContext = context;
        mRepository = CryptoWatchRepository.getInstance(CryptoWatchBaseApp.getDaoSession());
        mPortfolioListener = listener;
    }

    @NonNull
    @Override
    public FavouritesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_favourite_item, parent, false);
        return new FavouritesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull FavouritesViewHolder holder, int position) {
        final Coin coin = mCoins.get(position);
        setCoinNameView(holder.coinName, coin);
        setDeleteView(holder.delete, coin);
        setPortfolioView(holder.portfolio, coin);
    }

    private void setCoinNameView(final TextView coinName, final Coin coin) {
        coinName.setText(coin.getCoinName() + " (" + coin.getCoinSymbol() + ") ");
        coinName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, DetailsActivity.class);
                intent.putExtra(COIN_ID_EXTRA, coin.getId());
                mContext.startActivity(intent);
            }
        });
    }

    private void setDeleteView(final ImageView delete, final Coin coin) {
        delete.setImageResource(R.drawable.ic_delete);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                coin.setIsFavourite(false);
                mRepository.updateCoin(coin);
                mCoins.remove(coin);
                FavouritesAdapter.this.notifyDataSetChanged();
            }
        });
    }

    private void setPortfolioView (final ImageView portfolio, final Coin coin){
        if (coin.getIsInPortfolio()){
            portfolio.setImageResource(R.drawable.ic_selected_portfolio);
        } else {
            portfolio.setImageResource(R.drawable.ic_not_selected_portfolio);
        }

        portfolio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (coin.getIsInPortfolio()){

                    mPortfolioListener.onCoinInPortfolio(coin);
                } else {
                    mPortfolioListener.onCoinNotInPortfolio(coin);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mCoins.size();
    }

    public interface PortfolioListener {

        void onCoinInPortfolio(Coin coin);

        void onCoinNotInPortfolio(Coin coin);
    }

}
