package bogoslovov.kaloyan.cryprowatch.features.settings;

import bogoslovov.kaloyan.cryprowatch.data.repository.CryptoWatchDataSource;
import bogoslovov.kaloyan.cryprowatch.utils.Utils;

public class SettingsPresenter implements SettingsContract.Presenter{

    private static final String TAG = SettingsPresenter.class.getSimpleName();

    private final CryptoWatchDataSource mCryptoWatchRepository;
    private final SettingsContract.View mSettingsView;

    /**
     *
     * @param cryptoWatchRepository the repository contains all methods that access the database
     * @param settingsView the settings view
     */
    public SettingsPresenter(CryptoWatchDataSource cryptoWatchRepository, SettingsContract.View settingsView) {
        mCryptoWatchRepository = Utils.checkNotNull(cryptoWatchRepository, "cryptoWatchRepository cannot be null");
        mSettingsView = Utils.checkNotNull(settingsView,"settingsView cannot be null");

        mSettingsView.setPresenter(this);
    }

}