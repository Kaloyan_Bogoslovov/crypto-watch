package bogoslovov.kaloyan.cryprowatch.features.newposition;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import bogoslovov.kaloyan.cryprowatch.R;
import bogoslovov.kaloyan.cryprowatch.common.CryptoWatchBaseApp;
import bogoslovov.kaloyan.cryprowatch.data.db.DaoSession;
import bogoslovov.kaloyan.cryprowatch.data.repository.CryptoWatchRepository;
import bogoslovov.kaloyan.cryprowatch.utils.ActivityUtils;

import static bogoslovov.kaloyan.cryprowatch.utils.Constants.COIN_ID_EXTRA;
import static bogoslovov.kaloyan.cryprowatch.utils.Constants.COIN_NAME_EXTRA;

public class NewPositionActivity extends AppCompatActivity {

    private Bundle mCoinBundle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_position);

        if (savedInstanceState == null) {
            mCoinBundle = getIntent().getExtras();
        } else {
            mCoinBundle = savedInstanceState;
        }

        NewPositionFragment newPositionFragment =
                (NewPositionFragment) getFragmentManager().findFragmentById(R.id.contentFrame);

        if (newPositionFragment == null) {
            newPositionFragment = NewPositionFragment.newInstance();
            newPositionFragment.setArguments(mCoinBundle);
            ActivityUtils.addFragmentToActivity(getFragmentManager(), newPositionFragment, R.id.contentFrame);
        }

        DaoSession daoSession = CryptoWatchBaseApp.getDaoSession();

        new NewPositionPresenter(CryptoWatchRepository.getInstance(daoSession), newPositionFragment);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putLong(COIN_ID_EXTRA, mCoinBundle.getLong(COIN_ID_EXTRA));
        outState.putString(COIN_NAME_EXTRA, mCoinBundle.getString(COIN_NAME_EXTRA));
        super.onSaveInstanceState(outState);
    }
}