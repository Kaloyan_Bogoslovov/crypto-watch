package bogoslovov.kaloyan.cryprowatch.features.details;


import bogoslovov.kaloyan.cryprowatch.BaseView;

public interface DetailsContract {

    interface View extends BaseView<DetailsContract.Presenter> {
    }

    interface Presenter {

        void loadCoinById(Long id);

        String getTime(int lastUpdated);

        boolean isPriceMovementUpwards(Double priceChange);

        boolean isSupplyDataAvailable(Double circulatingSupply, Double maxSupply);
    }
}
