package bogoslovov.kaloyan.cryprowatch.features.settings;

import bogoslovov.kaloyan.cryprowatch.BaseView;

public interface SettingsContract {

    interface View extends BaseView<Presenter>{

    }

    interface Presenter {

    }
}
