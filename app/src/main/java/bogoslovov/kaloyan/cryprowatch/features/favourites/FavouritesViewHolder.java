package bogoslovov.kaloyan.cryprowatch.features.favourites;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import bogoslovov.kaloyan.cryprowatch.R;

public class FavouritesViewHolder extends RecyclerView.ViewHolder{

    public TextView coinName;
    public ImageView delete;
    public ImageView portfolio;

    public FavouritesViewHolder(View view) {
        super(view);

        coinName = view.findViewById(R.id.text_favourite_coin_name);
        delete = view.findViewById(R.id.button_delete_favourite_coin);
        portfolio = view.findViewById(R.id.button_portfolio);

    }
}
