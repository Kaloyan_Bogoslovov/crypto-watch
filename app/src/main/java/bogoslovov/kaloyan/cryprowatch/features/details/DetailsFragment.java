package bogoslovov.kaloyan.cryprowatch.features.details;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.hookedonplay.decoviewlib.DecoView;
import com.hookedonplay.decoviewlib.charts.SeriesItem;
import com.hookedonplay.decoviewlib.events.DecoEvent;

import org.greenrobot.eventbus.Subscribe;


import bogoslovov.kaloyan.cryprowatch.R;
import bogoslovov.kaloyan.cryprowatch.common.CryptoWatchBaseFragment;
import bogoslovov.kaloyan.cryprowatch.data.models.getcoinusd.CoinUsdModel;
import bogoslovov.kaloyan.cryprowatch.data.models.getcoinusd.UsdModel;
import bogoslovov.kaloyan.cryprowatch.utils.Constants;
import bogoslovov.kaloyan.cryprowatch.utils.Utils;

import static bogoslovov.kaloyan.cryprowatch.utils.Constants.COIN_DATA_NOT_AVAILABLE;
import static bogoslovov.kaloyan.cryprowatch.utils.Constants.COIN_LOADED;

public class DetailsFragment extends CryptoWatchBaseFragment implements DetailsContract.View{

    private static final String TAG = DetailsFragment.class.getSimpleName();

    private DetailsContract.Presenter mPresenter;
    private View mView;
    private LinearLayout mLoadingLayout;
    private TextView mLoadingText;
    private SwipeRefreshLayout mRefreshLayout;

    public DetailsFragment() {
    }

    public static DetailsFragment newInstance() {
        return new DetailsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_details, container, false);
        mLoadingLayout = mView.findViewById(R.id.progress_bar_layout);
        mLoadingText = mView.findViewById(R.id.text_loading);

        mRefreshLayout = mView.findViewById(R.id.swipe_layout);
        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadCoin();
            }
        });

        setToolbar();
        loadCoin();
        return mView;
    }

    @Override
    public void setPresenter(DetailsContract.Presenter presenter) {
        mPresenter = Utils.checkNotNull(presenter);
    }

    private void setToolbar() {
        setHasOptionsMenu(true);
        Toolbar toolbar = (Toolbar) mView.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void loadCoin(){
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            Long coinId = bundle.getLong(Constants.COIN_ID_EXTRA);
            mLoadingLayout.setVisibility(View.VISIBLE);
            mLoadingText.setText(getResources().getText(R.string.loading_coin));
            mPresenter.loadCoinById(coinId);
        }
    }

    @Subscribe
    public void onMessageEvent(DetailsEvent detailsEvent) {
        switch (detailsEvent.getEvent()){
            case COIN_LOADED:
                mLoadingLayout.setVisibility(View.GONE);
                mRefreshLayout.setRefreshing(false);
                onCoinLoaded(detailsEvent.getCoinUsdModel());
                break;
            case COIN_DATA_NOT_AVAILABLE:
                mLoadingText.setText(getResources().getText(R.string.loading_failed));
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("");
                ProgressBar loadingBar = mView.findViewById(R.id.progress_bar);
                loadingBar.setVisibility(View.GONE);
                mRefreshLayout.setRefreshing(false);
                break;
        }
    }

    private void onCoinLoaded(CoinUsdModel coinUsdModel) {
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(
                coinUsdModel.getDataModel().getName() + " ("+coinUsdModel.getDataModel().getSymbol()+")"
        );
        setHeadlineCard(coinUsdModel);
        setSupplyCard(coinUsdModel);
        setAdditionalInformationCard(coinUsdModel);
    }

    private void setHeadlineCard(CoinUsdModel coinUsdModel) {
        ImageView priceMovement = mView.findViewById(R.id.image_price_movement);
        TextView currentPrice = mView.findViewById(R.id.text_current_price);
        TextView priceChange = mView.findViewById(R.id.text_price_change);
        TextView lastUpdateTime = mView.findViewById(R.id.text_last_update_time);

        currentPrice.setText((String.valueOf(coinUsdModel.getDataModel().getQuotes().getUSD().getPrice())));
        priceChange.setText(" ("+coinUsdModel.getDataModel().getQuotes().getUSD().getPercentChange1h()+"%)");
        lastUpdateTime.setText(mPresenter.getTime(coinUsdModel.getDataModel().getLastUpdated()));

        if (mPresenter.isPriceMovementUpwards(coinUsdModel.getDataModel().getQuotes().getUSD().getPercentChange1h())) {
            priceMovement.setImageResource(R.drawable.ic_arrow_up);
            priceChange.setTextColor(getResources().getColor(android.R.color.holo_green_light));
        } else {
            priceMovement.setImageResource(R.drawable.ic_arrow_down);
            priceChange.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
        }
    }

    private void setSupplyCard(CoinUsdModel coinUsdModel) {

        Double circulatingSupplyValue = coinUsdModel.getDataModel().getCirculatingSupply();
        Double maxSupplyValue = coinUsdModel.getDataModel().getMaxSupply();

        TextView textView = mView.findViewById(R.id.text_percent_available_supply);
        TextView circulatingSupply = mView.findViewById(R.id.text_circulating_supply);
        TextView maxSupply = mView.findViewById(R.id.text_max_supply);

        DecoView decoView = mView.findViewById(R.id.dynamicArcView);
        decoView.configureAngles(260,0);
        decoView.addSeries(new SeriesItem.Builder(Color.argb(255, 218, 218, 218))
                .setRange(0, 100, 100)
                .setInitialVisibility(true)
                .setLineWidth(20f)
                .build());

        SeriesItem seriesItem = new SeriesItem.Builder(getResources().getColor(R.color.colorPrimary))
                .setRange(0, 100, 0)
                .setLineWidth(20f)
                .build();

        int seriesIndex = decoView.addSeries(seriesItem);

        if (mPresenter.isSupplyDataAvailable(circulatingSupplyValue, maxSupplyValue)){
            double percentCirculating = circulatingSupplyValue*100/maxSupplyValue;
            addProgressListener(seriesItem, textView, percentCirculating);
            addProgressListener(seriesItem, circulatingSupply, circulatingSupplyValue);
            addProgressListener(seriesItem, maxSupply, maxSupplyValue);
            decoView.addEvent(new DecoEvent.Builder(((float) percentCirculating)).setIndex(seriesIndex).setDelay(500).build());
        } else {
            addProgressListener(seriesItem, textView, 0);
            addProgressListener(seriesItem, circulatingSupply, 0);
            addProgressListener(seriesItem, maxSupply, 0);
            decoView.addEvent(new DecoEvent.Builder(0).setIndex(seriesIndex).setDelay(500).build());
        }

    }

    private void setAdditionalInformationCard(CoinUsdModel coinUsdModel) {
        UsdModel coin = coinUsdModel.getDataModel().getQuotes().getUSD();
        TextView priceChange1Hour = mView.findViewById(R.id.text_1h_change);
        TextView priceChange24Hours = mView.findViewById(R.id.text_24h_change);
        TextView priceChange7Days = mView.findViewById(R.id.text_7d_change);
        TextView volume = mView.findViewById(R.id.text_volume);
        TextView marketCap = mView.findViewById(R.id.text_market_cap);

        volume.setText(Utils.minimizeNumber(coin.getVolume24h()));
        marketCap.setText(Utils.minimizeNumber(coin.getMarketCap()));
        setPriceChangeTextView(priceChange1Hour, coin.getPercentChange1h());
        setPriceChangeTextView(priceChange24Hours, coin.getPercentChange24h());
        setPriceChangeTextView(priceChange7Days, coin.getPercentChange7d());
    }

    private void setPriceChangeTextView(TextView view, Double priceChange) {
        view.setText(String.valueOf(priceChange+"%"));
        if (mPresenter.isPriceMovementUpwards(priceChange)) {
            view.setTextColor(getResources().getColor(android.R.color.holo_green_light));
        } else {
            view.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
        }
    }

    private void addProgressListener(@NonNull final SeriesItem seriesItem, @NonNull final TextView view, final double coinSupply) {

        seriesItem.addArcSeriesItemListener(new SeriesItem.SeriesItemListener() {
            @Override
            public void onSeriesItemAnimationProgress(float percentComplete, float currentPosition) {
                if (coinSupply == 0) {
                  view.setText("N/A");
                } else if (coinSupply < 100) {
                    float percentFilled = (currentPosition - seriesItem.getMinValue()) / (seriesItem.getMaxValue() - seriesItem.getMinValue());
                    view.setText(String.format("%.0f%%", percentFilled * 100f));
                } else {
                    view.setText(Utils.minimizeNumber(percentComplete * coinSupply));
                }
            }

            @Override
            public void onSeriesItemDisplayProgress(float percentComplete) {
            }
        });
    }

}