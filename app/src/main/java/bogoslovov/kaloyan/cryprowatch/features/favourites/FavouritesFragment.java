package bogoslovov.kaloyan.cryprowatch.features.favourites;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import bogoslovov.kaloyan.cryprowatch.R;
import bogoslovov.kaloyan.cryprowatch.data.db.Coin;
import bogoslovov.kaloyan.cryprowatch.features.newposition.NewPositionActivity;
import bogoslovov.kaloyan.cryprowatch.features.search.SearchAdapter;
import bogoslovov.kaloyan.cryprowatch.utils.Utils;

import static bogoslovov.kaloyan.cryprowatch.utils.Constants.COIN_ID_EXTRA;
import static bogoslovov.kaloyan.cryprowatch.utils.Constants.COIN_NAME_EXTRA;

public class FavouritesFragment extends android.app.Fragment implements FavouritesContract.View, SearchAdapter.PortfolioListener{

    private static final String TAG = FavouritesFragment.class.getSimpleName();

    private FavouritesContract.Presenter mPresenter;
    private RecyclerView mRecyclerView;
    private TextView mEmptyView;
    private FavouritesAdapter mAdapter;

    public FavouritesFragment() {
    }

    public static FavouritesFragment newInstance() {
        return new FavouritesFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favourites, container, false);

        mRecyclerView = view.findViewById(R.id.recycle_view);
        mEmptyView = view.findViewById(R.id.empty_view);

        if (mPresenter!=null) mPresenter.loadFavouriteCoins();

        return view;
    }

    @Override
    public void setPresenter(FavouritesContract.Presenter presenter) {
        mPresenter = Utils.checkNotNull(presenter);
    }

    @Override
    public void setRecycleView(List<Coin> coins) {
        if (!coins.isEmpty()) {
            showRecycleView();
            mAdapter = new FavouritesAdapter(coins, getActivity(), this);
            mRecyclerView.setAdapter(mAdapter);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        } else {
            showEmptyView();
        }
    }

    private void showEmptyView() {
        mRecyclerView.setVisibility(View.GONE);
        mEmptyView.setVisibility(View.VISIBLE);
    }

    private void showRecycleView() {
        mEmptyView.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onCoinInPortfolio(Coin coin) {
        showRemoveCoinFromPortfolioDialog(coin);
    }

    private void showRemoveCoinFromPortfolioDialog(final Coin coin) {
        new AlertDialog.Builder(getActivity())
                .setMessage("Remove the coin from portfolio?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mPresenter.removePosition(coin.getId());
                        mAdapter.notifyDataSetChanged();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                }).create().show();

    }

    @Override
    public void onCoinNotInPortfolio(Coin coin) {
        Intent intent = new Intent(getActivity(), NewPositionActivity.class);
        intent.putExtra(COIN_NAME_EXTRA, coin.getCoinName() + " ("+ coin.getCoinSymbol() + ")");
        intent.putExtra(COIN_ID_EXTRA, coin.getId());
        startActivity(intent);
    }
}
