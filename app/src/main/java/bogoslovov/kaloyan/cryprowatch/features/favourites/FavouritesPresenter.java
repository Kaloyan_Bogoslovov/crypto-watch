package bogoslovov.kaloyan.cryprowatch.features.favourites;

import java.util.List;

import bogoslovov.kaloyan.cryprowatch.data.db.Coin;
import bogoslovov.kaloyan.cryprowatch.data.repository.CryptoWatchDataSource;
import bogoslovov.kaloyan.cryprowatch.data.repository.CryptoWatchRepository;
import bogoslovov.kaloyan.cryprowatch.utils.Utils;

import static bogoslovov.kaloyan.cryprowatch.utils.Constants.NO_PORTFOLIO_ID;

public class FavouritesPresenter implements FavouritesContract.Presenter{

    private static final String TAG = FavouritesPresenter.class.getSimpleName();

    private final CryptoWatchDataSource mCryptoWatchRepository;
    private final FavouritesContract.View mFavouritesView;

    /**
     *
     * @param cryptoWatchRepository the repository contains all methods that access the database
     * @param favouritesView the favourites view
     */
    public FavouritesPresenter(CryptoWatchRepository cryptoWatchRepository, FavouritesContract.View favouritesView) {
        mCryptoWatchRepository = Utils.checkNotNull(cryptoWatchRepository, "cryptoWatchRepository cannot be null");
        mFavouritesView = Utils.checkNotNull(favouritesView,"favouritesView cannot be null");

        mFavouritesView.setPresenter(this);
    }

    @Override
    public void loadFavouriteCoins() {
        List<Coin> coins = mCryptoWatchRepository.getFavouriteCoins();
        mFavouritesView.setRecycleView(coins);
    }

    @Override
    public void removePosition(Long coinId) {

        Coin coin = mCryptoWatchRepository.getCoinById(coinId).get(0);

        if (coin.getPortfolioId() != null && coin.getPortfolioId() != NO_PORTFOLIO_ID) {
            mCryptoWatchRepository.deletePositionById(coin.getPortfolioId());
        }

        coin.setIsInPortfolio(false);
        coin.setPortfolioId(NO_PORTFOLIO_ID);
        mCryptoWatchRepository.updateCoin(coin);
    }
}
