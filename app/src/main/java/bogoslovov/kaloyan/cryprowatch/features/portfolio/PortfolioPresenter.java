package bogoslovov.kaloyan.cryprowatch.features.portfolio;

import bogoslovov.kaloyan.cryprowatch.data.repository.CryptoWatchDataSource;
import bogoslovov.kaloyan.cryprowatch.utils.Utils;

public class PortfolioPresenter implements PortfolioContract.Presenter{

    private static final String TAG = PortfolioPresenter.class.getSimpleName();

    private final CryptoWatchDataSource mCryptoWatchRepository;
    private final PortfolioContract.View mPortfolioView;

    /**
     *
     * @param cryptoWatchRepository the repository contains all methods that access the database
     * @param portfolioView the portfolio view
     */
    public PortfolioPresenter(CryptoWatchDataSource cryptoWatchRepository, PortfolioContract.View portfolioView) {
        mCryptoWatchRepository = Utils.checkNotNull(cryptoWatchRepository, "cryptoWatchRepository cannot be null");
        mPortfolioView = Utils.checkNotNull(portfolioView,"portfolioView cannot be null");

        mPortfolioView.setPresenter(this);
    }

}