package bogoslovov.kaloyan.cryprowatch.features.newposition;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import bogoslovov.kaloyan.cryprowatch.CryptoWatchHomeActivity;
import bogoslovov.kaloyan.cryprowatch.R;
import bogoslovov.kaloyan.cryprowatch.common.CryptoWatchBaseFragment;
import bogoslovov.kaloyan.cryprowatch.utils.ActivityUtils;
import bogoslovov.kaloyan.cryprowatch.utils.Constants;
import bogoslovov.kaloyan.cryprowatch.utils.Utils;

public class NewPositionFragment extends Fragment implements NewPositionContract.View, View.OnClickListener {

    private static final String TAG = NewPositionFragment.class.getSimpleName();

    private NewPositionContract.Presenter mPresenter;
    private Long mCoinId;
    private View mView;
    private EditText mPrice;
    private EditText mAmount;

    public NewPositionFragment() {
    }

    public  static NewPositionFragment newInstance() {
        return new NewPositionFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_new_position, container, false);


        String coinName = null;
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            mCoinId = bundle.getLong(Constants.COIN_ID_EXTRA);
            coinName = bundle.getString(Constants.COIN_NAME_EXTRA);
        }

        mAmount = mView.findViewById(R.id.edit_text_amount);
        mPrice = mView.findViewById(R.id.edit_text_price);

        Button addPosition = mView.findViewById(R.id.button_add_position);
        addPosition.setOnClickListener(this);
        setToolbar(mView, coinName);

        return mView;
    }

    @Override
    public void setPresenter(NewPositionContract.Presenter presenter) {
        mPresenter = Utils.checkNotNull(presenter);
    }

    private void setToolbar(View view, String title) {
        setHasOptionsMenu(true);
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        if (title.isEmpty()){
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.toolbar_title_new_position));
        } else {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(
                    getResources().getString(R.string.toolbar_title_new_position_add)
                            + " "
                            + title
            );
        }

        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public void onPositionAdded() {
        Intent intent = new Intent(getActivity(), CryptoWatchHomeActivity.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        if (TextUtils.isEmpty(mAmount.getText().toString())) {
            Toast.makeText(getActivity(), "Please input an amount", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(mPrice.getText().toString())) {
            Toast.makeText(getActivity(), "Please input the purchasing price", Toast.LENGTH_SHORT).show();
        } else {
            Double amount = Double.valueOf(mAmount.getText().toString());
            Double price = Double.valueOf(mPrice.getText().toString());

            mPresenter.addNewPosition(amount, price, mCoinId);
        }



    }
}
