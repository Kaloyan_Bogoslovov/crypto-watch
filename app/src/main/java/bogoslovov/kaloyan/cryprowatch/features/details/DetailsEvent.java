package bogoslovov.kaloyan.cryprowatch.features.details;

import bogoslovov.kaloyan.cryprowatch.data.models.getcoinusd.CoinUsdModel;

public class DetailsEvent {

    private final int event;
    private final CoinUsdModel coinUsdModel;

    public DetailsEvent(int event, CoinUsdModel coinUsdModel) {
        this.event = event;
        this.coinUsdModel = coinUsdModel;
    }

    public int getEvent() {
        return event;
    }

    public CoinUsdModel getCoinUsdModel() {
        return coinUsdModel;
    }
}
