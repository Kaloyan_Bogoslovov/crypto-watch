package bogoslovov.kaloyan.cryprowatch.features.settings;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import bogoslovov.kaloyan.cryprowatch.R;
import bogoslovov.kaloyan.cryprowatch.features.favourites.FavouritesFragment;
import bogoslovov.kaloyan.cryprowatch.utils.Utils;

public class SettingsFragment extends Fragment implements SettingsContract.View{

    private static final String TAG = FavouritesFragment.class.getSimpleName();

    private SettingsContract.Presenter mPresenter;

    public SettingsFragment() {
    }

    public static SettingsFragment newInstance(){
        return new SettingsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        return view;
    }

    @Override
    public void setPresenter(SettingsContract.Presenter presenter) {
        mPresenter = Utils.checkNotNull(presenter);
    }
}
