package bogoslovov.kaloyan.cryprowatch.features.favourites;

import java.util.List;

import bogoslovov.kaloyan.cryprowatch.BaseView;
import bogoslovov.kaloyan.cryprowatch.data.db.Coin;

public interface FavouritesContract {

    interface View extends BaseView<Presenter>{

        void setRecycleView(List<Coin> coins);

    }

    interface Presenter {

        void loadFavouriteCoins();

        void removePosition(Long coinId);
    }
}
