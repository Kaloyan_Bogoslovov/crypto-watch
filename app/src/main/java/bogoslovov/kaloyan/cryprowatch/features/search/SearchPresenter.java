package bogoslovov.kaloyan.cryprowatch.features.search;

import java.util.List;

import bogoslovov.kaloyan.cryprowatch.data.db.Coin;
import bogoslovov.kaloyan.cryprowatch.data.jobs.CoinsJob;
import bogoslovov.kaloyan.cryprowatch.data.repository.CryptoWatchDataSource;
import bogoslovov.kaloyan.cryprowatch.utils.Utils;

import static bogoslovov.kaloyan.cryprowatch.utils.Constants.NO_PORTFOLIO_ID;

public class SearchPresenter implements SearchContract.Presenter{

    private static final String TAG = SearchPresenter.class.getSimpleName();

    private final CryptoWatchDataSource mCryptoWatchRepository;
    private final SearchContract.View mSearchView;

    /**
     *
     * @param cryptoWatchRepository the repository contains all methods that access the database
     * @param searchView the search view
     */
    public SearchPresenter(CryptoWatchDataSource cryptoWatchRepository, SearchContract.View searchView) {
        mCryptoWatchRepository = Utils.checkNotNull(cryptoWatchRepository, "cryptoWatchRepository cannot be null");
        mSearchView = Utils.checkNotNull(searchView,"searchView cannot be null");

        mSearchView.setPresenter(this);
    }

    @Override
    public void loadInitialCoins() {
        List<Coin> coins = mCryptoWatchRepository.getAllCoins();
        mSearchView.setRecycleView(coins);
    }

    @Override
    public void loadUpdatedCoins() {
        List<Coin> coins = mCryptoWatchRepository.getAllCoins();
        mSearchView.notifyAdapterForUpdatedCoins(coins);
    }

    @Override
    public void syncCoinsNow() {
        CoinsJob.scheduleJobNow();
    }

    @Override
    public void removePosition(Long coinId) {

        Coin coin = mCryptoWatchRepository.getCoinById(coinId).get(0);

        if (coin.getPortfolioId() != null && coin.getPortfolioId() != NO_PORTFOLIO_ID) {
            mCryptoWatchRepository.deletePositionById(coin.getPortfolioId());
        }

        coin.setIsInPortfolio(false);
        coin.setPortfolioId(NO_PORTFOLIO_ID);
        mCryptoWatchRepository.updateCoin(coin);
    }
}