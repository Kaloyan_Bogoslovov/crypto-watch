package bogoslovov.kaloyan.cryprowatch.features.search;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import bogoslovov.kaloyan.cryprowatch.R;
import bogoslovov.kaloyan.cryprowatch.data.db.Coin;
import bogoslovov.kaloyan.cryprowatch.features.favourites.FavouritesFragment;
import bogoslovov.kaloyan.cryprowatch.features.newposition.NewPositionActivity;
import bogoslovov.kaloyan.cryprowatch.utils.Utils;

import static bogoslovov.kaloyan.cryprowatch.utils.Constants.COINS_SYNC_FAILED;
import static bogoslovov.kaloyan.cryprowatch.utils.Constants.COINS_SYNC_SUCCESSFUL;
import static bogoslovov.kaloyan.cryprowatch.utils.Constants.COIN_ID_EXTRA;
import static bogoslovov.kaloyan.cryprowatch.utils.Constants.COIN_NAME_EXTRA;

public class SearchFragment extends android.app.Fragment implements SearchContract.View, SearchAdapter.PortfolioListener{

    private static final String TAG = FavouritesFragment.class.getSimpleName();

    private SearchContract.Presenter mPresenter;
    private RecyclerView mRecyclerView;
    private TextView mEmptyView;
    private SearchAdapter mAdapter;
    private String mSearchQuery;
    private SwipeRefreshLayout mRefreshLayout;

    public SearchFragment() {
    }

    public static SearchFragment newInstance(){
        return new SearchFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        setHasOptionsMenu(true);
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("");

        mRecyclerView = view.findViewById(R.id.recycle_view);
        mEmptyView = view.findViewById(R.id.empty_view);
        mRefreshLayout = view.findViewById(R.id.swipe_layout);

        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (mPresenter!=null) mPresenter.syncCoinsNow();
            }
        });

        if (mPresenter!=null) mPresenter.loadInitialCoins();

        return view;
    }

    @Override
    public void setPresenter(SearchContract.Presenter presenter) {
        mPresenter = Utils.checkNotNull(presenter);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.menu_search, menu);

        final MenuItem menuItem = menu.findItem( R.id.action_search);

        final SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.onActionViewExpanded();
        searchView.clearFocus();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                if (mAdapter != null){
                    mAdapter.setFilter(query);
                }

                mSearchQuery = query;
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if (mAdapter != null){
                    mAdapter.setFilter(s);
                }
                return false;
            }
        });

        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSearchQuery !=null){
                    searchView.setQuery(mSearchQuery, false);
                }
            }
        });
    }

    @Override
    public void setRecycleView(List<Coin> coins) {
        if (!coins.isEmpty()){
            showRecycleView();
            mAdapter = new SearchAdapter(coins, getActivity(), this);
            mRecyclerView.setAdapter(mAdapter);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        } else {
            showEmptyView();
        }
    }

    @Override
    public void notifyAdapterForUpdatedCoins(List<Coin> coins) {
        if (mAdapter == null){
            setRecycleView(coins);
        } else {
            showRecycleView();
            mAdapter.dataSetChanged(coins);
        }
        mRefreshLayout.setRefreshing(false);
        showSuccessfulSyncMessage();
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(SearchEvent event){
        switch (event.getEvent()){
            case COINS_SYNC_SUCCESSFUL:
                mPresenter.loadUpdatedCoins();
                break;
            case COINS_SYNC_FAILED:
                mRefreshLayout.setRefreshing(false);
                showFailureSyncMessage();
                break;
        }
    }

    private void showEmptyView(){
        mRecyclerView.setVisibility(View.GONE);
        mEmptyView.setVisibility(View.VISIBLE);
    }

    private void showRecycleView(){
        mEmptyView.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    private void showSuccessfulSyncMessage(){
        Toast.makeText(getActivity(), getResources().getText(R.string.message_coins_updated_successfully), Toast.LENGTH_LONG).show();
    }

    private void showFailureSyncMessage(){
        Toast.makeText(getActivity(), getResources().getText(R.string.message_coins_sync_failed), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onCoinInPortfolio(Coin coin) {
        showRemoveCoinFromPortfolioDialog(coin);
    }

    private void showRemoveCoinFromPortfolioDialog(final Coin coin) {
        new AlertDialog.Builder(getActivity())
                .setMessage("Remove the coin from portfolio?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mPresenter.removePosition(coin.getId());
                        mAdapter.notifyDataSetChanged();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                }).create().show();

    }

    @Override
    public void onCoinNotInPortfolio(Coin coin) {
        Intent intent = new Intent(getActivity(), NewPositionActivity.class);
        intent.putExtra(COIN_NAME_EXTRA, coin.getCoinName() + " ("+ coin.getCoinSymbol() + ")");
        intent.putExtra(COIN_ID_EXTRA, coin.getId());
        startActivity(intent);
    }
}
