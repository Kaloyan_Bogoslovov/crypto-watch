package bogoslovov.kaloyan.cryprowatch.features.newposition;

import bogoslovov.kaloyan.cryprowatch.data.db.Coin;
import bogoslovov.kaloyan.cryprowatch.data.db.Position;
import bogoslovov.kaloyan.cryprowatch.data.repository.CryptoWatchDataSource;
import bogoslovov.kaloyan.cryprowatch.utils.Utils;

public class NewPositionPresenter implements NewPositionContract.Presenter{

    private final CryptoWatchDataSource mCryptoWatchRepository;
    private final NewPositionContract.View mNewPositionView;

    public NewPositionPresenter(CryptoWatchDataSource cryptoWatchRepository, NewPositionContract.View newPositionView) {
        mCryptoWatchRepository = Utils.checkNotNull(cryptoWatchRepository, "cryptoWatchRepository cannot be null");
        mNewPositionView = Utils.checkNotNull(newPositionView, "newPositionView cannot be null");

        mNewPositionView.setPresenter(this);
    }

    @Override
    public void addNewPosition(double amount, double price, Long coinId) {

        Coin coin = mCryptoWatchRepository.getCoinById(coinId).get(0);

        if (!coin.getIsInPortfolio()) {
            Position position = new Position();
            position.setAmountPurchased(amount);
            position.setPurchasedAtPrice(price);
            position.setCoinName(coin.getCoinName());
            position.setCoinSymbol(coin.getCoinSymbol());
            position.setCoinServerId(coin.getServerId());
            long positionId = mCryptoWatchRepository.insertPosition(position);

            coin.setIsInPortfolio(true);
            coin.setPortfolioId(positionId);
            mCryptoWatchRepository.updateCoin(coin);

            mNewPositionView.onPositionAdded();
        }
    }
}
