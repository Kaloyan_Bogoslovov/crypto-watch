package bogoslovov.kaloyan.cryprowatch.features.search;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import bogoslovov.kaloyan.cryprowatch.R;

public class SearchViewHolder extends RecyclerView.ViewHolder{

    public TextView coinName;
    public ImageView favourites;
    public ImageView portfolio;

    public SearchViewHolder(View view) {
        super(view);

        coinName = view.findViewById(R.id.text_coin_name);
        favourites = view.findViewById(R.id.button_favourites);
        portfolio = view.findViewById(R.id.button_portfolio);
    }

}
