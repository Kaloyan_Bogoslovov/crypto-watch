package bogoslovov.kaloyan.cryprowatch.features.portfolio;

import bogoslovov.kaloyan.cryprowatch.BaseView;

public interface PortfolioContract {

    interface View extends BaseView<Presenter>{

    }

    interface Presenter {

    }
}
