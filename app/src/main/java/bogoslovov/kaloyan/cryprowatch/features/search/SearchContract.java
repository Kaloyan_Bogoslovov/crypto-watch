package bogoslovov.kaloyan.cryprowatch.features.search;

import java.util.List;

import bogoslovov.kaloyan.cryprowatch.BaseView;
import bogoslovov.kaloyan.cryprowatch.data.db.Coin;

public interface SearchContract {

    interface View extends BaseView<Presenter> {

        void setRecycleView(List<Coin> coins);

        void notifyAdapterForUpdatedCoins(List<Coin> coins);

    }

    interface Presenter {

        void loadInitialCoins();

        void loadUpdatedCoins();

        void syncCoinsNow();

        void removePosition(Long coinId);
    }
}
