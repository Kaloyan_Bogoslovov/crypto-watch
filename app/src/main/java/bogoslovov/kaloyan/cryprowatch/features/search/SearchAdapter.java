package bogoslovov.kaloyan.cryprowatch.features.search;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import bogoslovov.kaloyan.cryprowatch.R;
import bogoslovov.kaloyan.cryprowatch.common.CryptoWatchBaseApp;
import bogoslovov.kaloyan.cryprowatch.data.db.Coin;
import bogoslovov.kaloyan.cryprowatch.data.repository.CryptoWatchDataSource;
import bogoslovov.kaloyan.cryprowatch.data.repository.CryptoWatchRepository;
import bogoslovov.kaloyan.cryprowatch.features.details.DetailsActivity;

import static bogoslovov.kaloyan.cryprowatch.utils.Constants.COIN_ID_EXTRA;

public class SearchAdapter extends RecyclerView.Adapter<SearchViewHolder>{

    private List<Coin> mCoins;
    private List<Coin> mAllCoinsInDatabase;
    private Context mContext;
    private CryptoWatchDataSource mRepository;
    private PortfolioListener mPortfolioListener;

    public SearchAdapter(List<Coin> coins, Context context, PortfolioListener listener) {
        mCoins = coins;
        mContext = context;
        mAllCoinsInDatabase = coins;
        mRepository = CryptoWatchRepository.getInstance(CryptoWatchBaseApp.getDaoSession());
        mPortfolioListener = listener;
    }

    @NonNull
    @Override
    public SearchViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_search_item, parent, false);
        return new SearchViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchViewHolder holder, int position) {
        final Coin coin = mCoins.get(position);

        holder.coinName.setText(coin.getCoinName() + " (" + coin.getCoinSymbol() + ") ");
        holder.coinName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, DetailsActivity.class);
                intent.putExtra(COIN_ID_EXTRA, coin.getId());
                mContext.startActivity(intent);
            }
        });

        setFavouriteView(holder.favourites, coin);
        setPortfolioView(holder.portfolio, coin);
    }

    private void setFavouriteView(final ImageView favourites,final Coin coin){
        if (coin.getIsFavourite()){
            favourites.setImageResource(R.drawable.ic_selected_favourites);
        } else {
            favourites.setImageResource(R.drawable.ic_not_selected_favourites);
        }

        favourites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (coin.getIsFavourite()){
                    favourites.setImageResource(R.drawable.ic_not_selected_favourites);
                    coin.setIsFavourite(false);
                    mRepository.updateCoin(coin);
                } else {
                    favourites.setImageResource(R.drawable.ic_selected_favourites);
                    coin.setIsFavourite(true);
                    mRepository.updateCoin(coin);
                }
            }
        });
    }

    private void setPortfolioView (final ImageView portfolio, final Coin coin){
        if (coin.getIsInPortfolio()){
            portfolio.setImageResource(R.drawable.ic_selected_portfolio);
        } else {
            portfolio.setImageResource(R.drawable.ic_not_selected_portfolio);
        }

        portfolio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (coin.getIsInPortfolio()){

                    mPortfolioListener.onCoinInPortfolio(coin);
                } else {
                    mPortfolioListener.onCoinNotInPortfolio(coin);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mCoins.size();
    }

    public void setFilter(String query){
        mCoins = new ArrayList<>();
        if (mAllCoinsInDatabase.size() > 0) {
            for (int i = 0; i < mAllCoinsInDatabase.size(); i++) {
                if (mAllCoinsInDatabase.get(i).getCoinName().toLowerCase().contains(query.toLowerCase())) {
                    mCoins.add(mAllCoinsInDatabase.get(i));
                }
            }
        }

        this.notifyDataSetChanged();
    }

    public void dataSetChanged(List<Coin> coins){
        mCoins = coins;
        mAllCoinsInDatabase = coins;
        this.notifyDataSetChanged();
    }

    public interface PortfolioListener {

        void onCoinInPortfolio(Coin coin);

        void onCoinNotInPortfolio(Coin coin);
    }

}
