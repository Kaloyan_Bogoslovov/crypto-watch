package bogoslovov.kaloyan.cryprowatch.common;

import android.app.Fragment;

import org.greenrobot.eventbus.EventBus;

public class CryptoWatchBaseFragment extends Fragment {

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }
}
