package bogoslovov.kaloyan.cryprowatch.common;

import android.app.Application;

import com.evernote.android.job.JobManager;

import org.greenrobot.greendao.database.Database;

import java.net.URISyntaxException;

import bogoslovov.kaloyan.cryprowatch.data.db.DaoMaster;
import bogoslovov.kaloyan.cryprowatch.data.db.DaoSession;
import bogoslovov.kaloyan.cryprowatch.data.jobs.CoinsJob;
import bogoslovov.kaloyan.cryprowatch.data.jobs.CryptoWatchJobCreator;

public class CryptoWatchBaseApp extends Application{

    private static DaoSession mDaoSession;

    @Override
    public void onCreate() {
        super.onCreate();

        //initializing the job creator
        JobManager.create(this).addJobCreator(new CryptoWatchJobCreator());

        //initialize the coin
        CoinsJob.scheduleJobNow();

        //initializing the db
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "crypto-watch-db");
        Database db = helper.getWritableDb();
        mDaoSession = new DaoMaster(db).newSession();
    }

    public static DaoSession getDaoSession(){
        return mDaoSession;
    }


    public static class Permissions {
        public static boolean localLog = true;
    }

}
