package bogoslovov.kaloyan.cryprowatch;

public interface BaseView<T> {

    void setPresenter(T presenter);

}
